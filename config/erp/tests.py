from django.test import TestCase
from config.wsgi import *
#from erp.models import Type, Employee, Category
from erp.models import Category

# Create your tests here.

# Listar en SQL: SELECT * from Tabla

# Listar en ORM (Mapeo de objetos relacionales)

# query = Type.objects.all()
# print(query)

# Insertar registro en class Type de models
# t = Type()
# t.name = "Presidente"
# t.save()
# t = Type(name='No Administrativo').save()

# Edicion de un  registro (si lo duplico, que no se detenga el programa con try: exception:)
# try:
#     t = Type.objects.get(id=5)
#     t.name = "No Administrativos"
#     t.save()
#
# except Exception as e:
#     print(e)


# Eliminar
#t = Type.objects.get(pk=3)
#t.delete()

# Filtrar por uno o varios atributos = filter
# obj = Type.objects.filter(name__icontains='Side') # name__icontains trae may o minusc = Presidente
# obj = Type.objects.filter(name__endswith="s") # Accionistas, Empleados
# obj = Type.objects.filter(name__startswith="E") # Empleados
# obj = Type.objects.filter(name__in=['No Administrativo', 'Presidente']) # No Adm, Presidente
# obj = Type.objects.filter(name__in=['No Administrativo', 'Presidente']).count() # 2
# obj = Type.objects.filter(name__exact="Accionistas").query # Consulta SQL
# obj = Type.objects.filter(name__endswith="s").exclude(id=1) # Empleados
# obj = print(obj)
#
# for i in Type.objects.filter(name__endswith="s")[0:]: # Empleados, No Administrativos
#     print(i.name)
#obj = Employee.objects.filter(type_id=1,date_creation__range=) # No tendo registros, traería Empleados Administrativos
# for i in Type.objects.filter(employee__age=25) # S/Reg, traeria Empleados con 25 años
#     print(i.name)
# for i in Type.objects.filter(employee__age=25) # S/Reg, traeria Empleados con 25 años
#     print(i.name)
print(Category.objects.all())

for i in Category.objects.filter():
    print(i)
