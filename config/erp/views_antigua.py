from django.http import HttpResponse,JsonResponse
from django.shortcuts import render

from erp.models import Category,Product


# Create your views here.
def myfirstview(request):
    data = {'name':'Sergio'}
    #return HttpResponse("Hola esta es mi primer URL")
    return JsonResponse(data)

def index(request):
    data = {'name':'Mario',
            'categories': Category.objects.all()
            }
    return render(request, 'base.html', data)


def mysecondview(request):
    data = {'name':'Mochis',
            'products': Product.objects.all()
            }
    return render(request, 'second_page.html',data )